# Lenguajes y Paradigmas de Programación
## Hoja de Problemas
### Tema 4

<!-- Escuela Superior de Ingeniería Informática
Lenguajes y Sistemas Informáticos
Universidad de La Laguna -->

1. ¿Qué es un bloque en Ruby?:star:

    >Los Bloques de código (llamados cierres en otros Lenguajes) son trozos de código que se encuentran entre llaves **{}** o entre **do...end** que se pueden asociar con invocaciones de método, casi como si fueran parámetros. Se puede considerar como una función anónima que se le pasa como argumento a un método.

2. ¿Qué se obtiene como salida? Describa el comportamiento del programa.:star:

    ```ruby
    class MyArray < Array
        def initialize (a_array)
            super(a_array)
        end

        def times_repeat (a_num)
            a_num.times do |num|
                self.each do |i|
                    yield "[#{num}] :: '#{i}'"
                end
            end
        end
    end

    a_array = MyArray.new([1,2,3])
    a_array.times_repeat(2) do |x|
        puts x
    end
    ```
    **SALIDA**
    ```
    [0] :: '1'
    [0] :: '2'
    [0] :: '3'
    [1] :: '1'
    [1] :: '2'
    [1] :: '3'
    ```
    >Se crea una clase hija de Array llamada MyArray. Se define el initialize de la clase hija, que básicamente, recibe un objeto array y con super, se llama al initialize de la clase madre Array.

    >Se define el método `times_repeat (a_num)` para MyArray. `.times` itera a_num veces, en este caso dos veces. Con `do |num|` le pasamos al yield la iteracion que llevamos, en este caso la iteración cero y después la 1.

    >Para cada uno de los elementos del objeto de la clase MyArray invocante (`self.each`) el elemento se envía al yield como `|i|`. En el yield se formatea la salida como `"[#{num}] :: '#{i}'"` que es lo que se muestra con el `puts x`.

3. ¿Qué es un Proc?:star:

    >Es una clase de Ruby que nos permite definir un bloque como un objeto.

    >Un bloque en Ruby se puede convertir en un objeto de la clase Proc si realizamos lo siguiente: `procedure = Proc new { #bloque de código }`. Después para realizar la invocación pertinente `procedure.call`.

    >En un objeto de la clase Proc no se comprueban los argumentos. Si se invoca al bloque con más parámetros que los que se le puede pasar, coge los argumentos que necesita(si faltan argumentos los pone a nil).

4. ¿Qué se obtiene como salida? Describa el comportamiento del programa.:star:

    ```ruby
    def a_method (a_block_argument)
        x = "Bye"
        a_block_argument.call
    end

    x = "Hello"
    a_block = Proc.new {puts x}

    a_method(a_block)
    ```
    **SALIDA**
    ```
    Hello
    ```

    >a_block es una closure (una función que tiene la habilidad de almacenar los valores de las variables locales dentro del ámbito en el cual el bloque fué creado, es decir, el ámbito nativo del bloque) al ser invocada dentro del método a_method mantiene el valor de x = “Hello” de su ambiente, por eso lo que se muestra por pantalla es Hello y no Bye. Si por ejemplo ejecutamos ese mismo código sin la línea 5, es decir sin declarar la variable x en su ambiente daría un error.

5. ¿Qué es Lambda?:star:

    > Otra forma de instanciar bloques también perteneciente a la clase Proc, que se comporta más como un método.

    >Se diferencia de un Proc en que un Lambda si comprueba los argumentos y en el tipo de retorno.


6. ¿Qué se obtiene como salida? Describa el comportamiento del programa.:star:

    ```ruby
    def a_lambda( arg )
        return lambda {|i| i * arg}
    end

    a = a_lambda(0.10)
    b = a_lambda(0.175)

    puts a.call(10)
    puts b.call(10)
    ```
    **SALIDA**
    ```
    1.0
    1.75
    ```
    >El método a_lambda lo que hace es retornar una lambda que recibe un argumento `i` y que devuelve ese multiplicado por el argumento que se le pasa en su construcción (arg). Es decir, que la lambda que queda almacenada en `a`,recibe un argumento `i` y devuelve el resultado de multiplicar `i` por `0.10`. En el ejemplo de la llamada (linea 7) se le pasa a la lambda `a` el argumento 10 (`i`) y 	se multiplica por `0.10` ,lo cual da como resultado 1. La lambda `b`funciona exactamente igual.


7. ¿Cómo se define una **clousure**?:star:

    >Es una función que tiene la habilidad de almacenar los valores de las variables locales dentro del ámbito en el cual el bloque fué creado, es decir, el ámbito nativo del bloque.

8. ¿Qué se obtiene como salida? :star:

    ```ruby
    def tutu(n,m,&b)
        b.call()
    end
    # => nil
    tutu 2, {:a => 1, :b => 2} {puts "hello"} #Imprime hello
    # => hello
    tutu(2,{:a => 1, :b=>2}) {puts "hello"}
    # => hello
    ```

9. Considere el siguiente código Ruby :star:

    ```ruby
    array = [1,2,3,4]

    array.collect! do |n|
        n ** 2
    end

    puts array
    ```
    * Escriba el código de un método denominado iterador! que se comporte de la misma forma utilizando *yield*.

    ```ruby
    class Array
	     def iterator!
	         self.each_with_index do |i,n|
	            self[i]=yield n
	      end
      end
	  end
	array.iterador! { |x| x**2 }
    ```
    * Repita el ejercicio anterior utilizando un objeto *Proc*.

    ```ruby
    class Array
  	   def iterator!(&proc)
  	      self.each do |i,n|
  	      self[i]= proc.call(n)
  	  end
  	end
  	array.iterador! { |x| x**2 }

    ```

10. ¿Qué se obtiene como salida? Describa el comportamiento del programa. :star:

    ```ruby
    class AClass
        def a_method(a_block)
            @hello = "I say"
            puts "[ In AClass.a_method ]"
            puts "in #{self} object of class #{self.class}, @hello = #{@hello}"
            puts  "[ In AClass.a_method ] when block is called..."
            a_block.call
        end
    end

    a_closure = lambda {
        @hello << " append!"
        puts "in #{self} object of class #{self.class}, @hello = #{@hello}"
    }

    def a_method(a_closure)
        @hello = "hello"
        a_closure.call
    end

    a_method(a_closure)
    data = AClass.new
    data.a_method(a_closure)
    ```
    **SALIDA**

    ```
    in main object of class Object, @hello = hello append!

    [ In AClass.a_method ]
    in #<AClass:0x007fac8380d040> object of class AClass, @hello = I say
    [ In AClass.a_method ] when block is called...
    in main object of class Object, @hello = hello append! append!
    ```
	>Cuando se muestran los puts aparecen los hello correspondientes al
	ámbito en el que nos encontramos. Pero cuando se hace la llamada a
	a_block.call el ello que se nos muestra es el hello de a_closure.

11. Considere el siguiente código Ruby :star:

    ```ruby
    def multiplier(n)
        lambda do |*arr|
            arr.collect { |i| i*n }
        end
    end
    ```
    * ¿Qué devuelve multiplier?
    >El método `multiplier` recibe argumento `n` y devuelve una lambda. Esta lambda recibirá una lista de argumentos, los convertirá a array, y devolverá el array con cada uno de sus elementos multiplicado por el argumento `n` que se le pasó a `multiplier`.

    * Proponga un ejemplo de uso del objeto devuelto por multiplier
    ```ruby
    a_lambda = multiplier(2)
  	a_lambda.call(1, 2, 3)
  	=> [2, 4, 6]
    ```

12. ¿Qué se obtiene como salida?

    ```ruby
    def a_method
        yield 2
    end

    x = 1
    a_method{ |x| }
    puts x
    ```
    **SALIDA**
    ```
    1
    ```
    > El bloque que se le pasa a 'a_method' no realiza ninguna operación (sólo declara un argumento para él mismo). Además, la |x| no es una variable sino el argumento del bloque (que tomará valor 2 [yield 2],	aunque no se haga nada más con él). La variable 'x=1' queda igual, finalmente.

13. ¿Qué se entiende por efectos laterales?:star:

    > Es cualquier cambio que una función produce fuera de su ámbito.

14. ¿Qué son las funciones de orden superior *(high order functions)*?:star:

    >Aquellas funciones que reciben una función como argumento y devuelven otra función como resultado.

15. ¿Qué es la memorización *(memorization)*?:star:

    >Es una técnica usada para acelerar la velocidad de cómputo de funciones puras que consiste en cachear y reutilizar cálculos ya efectuados, ya que sabemos que las funciones puras, para una misma lista de argumentos siempre devolverá el mismo resultado.

16. Un principio para hacer programación funcional en Ruby es *no actualizar* las variables sino *crearlas*. Si se necesita cambiar un objeto, no hay que modificarlo, hay que crear uno nuevo. ¿Qué opción del siguiente código sigue este principio?:star:
    * Añadir elementos a un *array* o cadena:
        1. Ejemplo **A**

        ```ruby
        indexes = [1,2,3]
        indexes << 4 #[1,2,3,4]
        ```
        2. Ejemplo **B**

        ```ruby
        indexes = [1,2,3]
        all_indexes = indexes + [4] #[1,2,3,4]
        ```
    * Actualización de hashes:
        1. Ejemplo **A**

        ```ruby
        hash={:a=>1,:b=>2}
        hash[:c] = 3
        ```
        2. Ejemplo **B**

        ```ruby
        hash={:a=>1,:b=>2}
        new_hash = hash.merge(:c => 3)
        ```
    * Modificaciones sobre un objeto:
        1. Ejemplo **A**

        ```ruby
        string = "LPP"
        string.gsub!(/P/,’p’) #"Lpp"
        ```
        2. Ejemplo **B**

        ```ruby
        string = "LPP"
        new_string = string.gsub(/P/,’p’) #"Lpp"
        ```
    * Acumulación de valores:
        1. Ejemplo **A**

        ```ruby
        output = [] output << 1
        output << 2 if is_it_needed?
        output << 3
        ```
        2. Ejemplo **B**

        ```ruby
        output = [1, 2 if is_it_needed?, 3].compact
        ```
    * Reutilización de variables:
        1. Ejemplo **A**

        ```ruby
        number = gets
        number = number.to_i
        ```
        2. Ejemplo **B**

        ```ruby
        number_string = gets
        number = number_string.to_i
        ```
      >En todas las preguntas la respuesta es la 2.

17. Un principio para hacer programación funcional en Ruby es utilizar bloques (*blocks*) pero si existe una función para hacer lo mismo, se ha de utilizar la función. ¿Qué opción del siguiente código sigue este principio?:star:
    * (init-empy + each + push) o map
    * (init-empy + each + conditional push) o (select/reject)
    * (initialize + each + accumulate) o inject
    * (empty + each + accumulate + push) o scan

    >En todas las preguntas la respuesta es la 2

18. Escriba un método **Ruby flip** que reciba como argumentos dos *lambdas* y cambie el orden de los parámetros.:star:

    ```ruby
    def flip(lambda1,lambda2)
      return lambda2,lambda1
    end
    ```

19. Escriba un método Ruby negate que reciba como argumento un lambda y niege el predicado que recibe como parámetro.:star:

    ```ruby
      def negate(lambda)
        return !lambda
      end
    ```
20. Escriba un método Ruby compose que reciba como argumentos dos lambdas y retorne la composición de las mismas.:star:

    ```ruby
      def compose (f,g)
        lambda { |x| f.call(g.call(x)) }
      end
    ```

21. Escriba una función Ruby haciendo uso de funciones de orden superior que calcule la suma de los diez primeros números naturales cuyos cuadrados son múltiplos de cinco.

  ```ruby
    (1..Float::INFINITY).lazy.select{|x| (x*x)%5==0}.fist(10).inject(0){|suma,num| suma+num}
  ```

22. Escriba una función Ruby haciendo uso de funciones de orden superior que calcule el producto de los cien primeros
numeros pares.

  ```ruby
  (1..Float::INFINITY).lazy.select{|x| x%2==0}.fist(100).inject(1){|mult,num| mult*num}
  ```

23. Escriba una función en Ruby haciendo uso de funciones de orden superior que calcule el máximo de los cien primeros números naturales.

  ```ruby
  (1..Float::INFINITY).first(100).max
  ```
    24. Escriba una función Ruby haciendo uso de funciones de orden superior que calcule el mínimo de
    los cien primeros números naturales.

    ```ruby
    (1..Float::INFINITY).first(100).min
    ```

    25. Escriba una función Ruby haciendo uso de funciones de orden superior que seleccione los números
    pares de entre los cien primeros números naturales.

    ```ruby
    (1..Float::INFINITY).first(100).select{|x| x.even?}
    ```

    26. Escriba una función Ruby haciendo uso de funciones de orden superior que seleccione los numeros
    impares de entre los cien primeros números naturales.

    ```ruby
    (1..Float::INFINITY).first(100).select{|x| x.odd?}
    ```

    27. ¿Que es la reflexión o instrospección?:star:

    >La habilidad de un lenguaje de programación para ser su propio metalenguaje.

    28. ¿Que es un contexto (binding)?:star:

    >Es un objeto donde se guarda toda la información necesaria para poder ejecutar el método (información del entorno donde se crea) ,incluyendo el valor del self y , si existe , el bloque que será ejecutado con yield.

    29. ¿Qué se entiende por metaprogramación?:star:

    >La metaprogramación consiste en escribir programas que ayudan a crear otros programas, así como sus datos.La metaprogramación es el conjunto de técnicas que permiten extender la sintaxis de Ruby para hacer más fácil la programación.

    30. ¿Que es un Lenguaje de Dominio especıfico (DSL -Domain Specific Language)?:star:

    >Es un lenguaje de programación o especificación de un lenguaje dedicado a resolver un problema en particular; representar un problema específico	y proveer una técnica para solucionar ese problema.

    31. ¿Qué diferencias hay entre instance_eval, class_eval y eval?

    •	`eval` permite evaluar el código contenido en un objeto de tipo String. También admite como segundo argumento un objeto Binding que provee el contexto para evaluar la cadena.
    •	`class_eval` define métodos de instancia.
    •	`instance_eval` define métodos singleton del objeto.


    32. ¿Cuál es el peligro de usar eval en un entorno distribuido?

    >En un entorno distribuido , los datos pueden venir de fuentes externas y si no se comprueba la bondad de las mismas estas dando acceso a tu maquina siendo inseguro. eval no evalúa riesgos, solo evalúa el string y lo ejecuta.

    33. Cuando se define un método dentro de un bloque o cadena llamado con class_eval ¿qué clase de método se está creando?

    >Un método de instancia ya que class_eval, además de ajustar self al objeto que deseemos, nos permite trabajar
    como si el bloque a evaluar estuviese en la definición de la clase, es decir que al crear un método
    en dicho entorno se crea un método de instancia.

    34. Cuando se define un método dentro de un bloque o cadena llamado con instance_eval? ¿qué clase de metodo se esta creando?

    >Un método singleton, ya que instance_eval, además de ajustar self al objeto que deseemos, nos permite trabajar como si el bloque a evaluar estuviese en la eigenclass de self, creando métodos singleton.

    35. ¿Cómo se puede incorporar un método definido en una cadena x = ’def met; "a method"; end'
    como método de clase, en una cierta clase A?

    >Usando A.instance_eval(x), ya que instance_eval crear métodos singleton de A. Al usar instance_eval sobre A, self pasa a ser A para el bloque o cadena que le pasemos, con lo cual los métodos singleton	creados en la eigenclass  de la clase son métodos de clase.

    36. Sea M un modulo y C una clase. ¿Qué ocurre si se ejecuta C.extend(M)? ¿Dónde acaban los
    métodos de instancia de M?

    >Al ejecutar el método `extend` sobre la clase C pasando como argumento un módulo M , los métodos de instancia de este módulo se convierten en métodos de clase (métodos singleton) de la clase C. Esto difiere de `include`, cuyo propósito es añadir los métodos de instancia del módulo a la clase como métodos de instancia también.

    37. Considere el siguiente fragmento de código en el que se llama repetidas veces a element:
    ```ruby
    class HTMLForm < XMLGrammar
      element :form, :action => REQ, # atributo requerido
              :method => "GET", # cadena: valor por defecto
              :enctype => "application/x-www-form-urlencoded",
              :name => OPT # opcional
      element :button, :name => OPT, :value => OPT,
              :type => "submit", :disabled => OPT
      element :br
      end
    ```
    * ¿Cuantos argumentos espera element? ¿Por qué?
        >Espera un argumento obligatorio (un tag) y otro opcional(un hash de atributos legales). Esto se puede saber porque en las tres llamadas a element el primer argumento siempre es un symbol y el segundo en una llamada no se indica y en otras sí.

    * ¿De qué clase son los argumentos?
        >El primero es un symbol y el opcional es un hash.

    * ¿Existen argumentos opcionales?
        >Los argumentos opcionales son los hash.

    * ¿El método element es de clase o de instancia?
        >Es un método de clase puesto que self está implícitamente llamando a la clase padre de HTMLForm (XMLGrammar).


38. ¿En qué consiste la integración continua (Continuous Integration)?

    >Es un modelo informático que consiste en hacer integraciones automáticas de un proyecto lo más a menudo posible para así poder detectar fallos cuanto antes. Consiste en realizar un conjunto de pruebas de forma automática en distintas plataformas y versiones de Ruby cada vez que se incorpora una funcionalidad nueva en el código.

39. Describa el conjunto de pasos a seguir para desarrollar una aplicación con Travis.

    >1-Iniciar sesión en Travis CI con la cuenta de GitHub,aceptando los permisos de confirmación.

    >2-Activar TravisCI para el repositorio que se quiere construir (enlazar con nuestro repositorio que deseemos). Es decir, dar permiso a Travis para entrar a nuestro repositorio.

  	>3-Agregar el fichero .travis.yml al repositorio para decir a TravisCI que construir (especificando las versiones que se quieren verificar).

    >4-Incluir las gemas a utilizar en el fichero Gemfile. (gem travis)

    >5-Agregar el fichero .travis.yml a git, hacer commit y push para construir un proyecto Travis CI.

    >6-Cada vez que se haga un push, se va a lanzar una tarea de Travis con las versiones a comprobar.

40. ¿En qué directorio hay que implementar los requerimientos de Travis?

    >En el directorio raíz del proyecto.

41. ¿Que se ha de especificar en el fichero.travis.yml?

    >Listado de plataformas y versiones en las que se desea probar la aplicación.

42. Describa el contenido de un fichero Gemfile utilizado por Travis.

    >development.dependency 'Travis'


43. ¿Como se pone en funcionamiento Travis?

    >Haciendo git commit y git push a Github para actualizar los cambios.

44. ¿Qué es Coveralls?

    >Coveralls es una herramienta que trabaja con Travis para la historia del cubrimiento de pruebas y las estadísticas del código.

45. Describa el conjunto de pasos a seguir para desarrollar una aplicación con Coveralls.

    >1-Registrarse en Coveralls.

    >2-Acceder a la cuenta de Coveralls y habilitar los accesos a los repositorios que se quieren usar.

	  >3-Añadir el fichero .coveralls.yml al directorio raíz del proyecto y añadir el token espeficicado.

	  >4--Incluir las gemas a utilizar en el fichero Gemfile. (gem coveralls)

46. ¿En qué directorio hay que implementar los requerimientos de Coveralls?

    >En el directorio raíz del proyecto.

47. ¿Cómo se pone en funcionamiento Coveralls?

    >coveralls open ( se debe tener el fichero .coveralls.yml con el repo_token dentro).

48. ¿Qué se ha de especificar en el fichero .coveralls.yml?

    >El token del repositorio de Coveralls.

49. Describa el contenido de un fichero Gemfile utilizado por Coveralls.

    >gem 'coveralls' : Contiene las dependencias de Coveralls.
