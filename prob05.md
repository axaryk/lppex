# Lenguajes y Paradigmas de Programación
## Hoja de Problemas
### Tema 5

<!-- Escuela Superior de Ingeniería Informática
Lenguajes y Sistemas Informáticos
Universidad de La Laguna -->

>Nota: Las cuestiones que te preguntan qué se obtiene de salida en el programa, no es necesario indicar la salida exacta como si tuvieras delante una terminal; hay que describirlo con palabras.

1. ¿Que es un Thread?:star:

    >Es  un objeto que representa un hilo de ejecución y permite ejecutar varios bloques de código de forma simultánea en distintos hilos dentro de un mismo programa. Es una forma ligera y eficiente para lograr la concurrencia en el código.

2. ¿Como se crean los Thread en Ruby?:star:

    ```ruby
    hilo = Thread.new { bloque del hilo }
    ```

3. ¿Que métodos proporciona Ruby para la manipulación de Threads?:star:

    >```Thread.new```: Método para crear un nuevo hilo.

    >```Thread#join```: Método que bloquea el hilo que realiza la llamada de esta sentencia hasta que no termine la ejecución del hilo que llama a join.

    >```Thread.current```: Método que devuelve el objeto Thread que representa el hilo actual.

    >```Thread#kill```: Método que termina un hilo, igual que ```Thread.exit```.

    >```Thread.main```: Método que retorna el objeto Thread que representa el hilo principal.

    >```Thread#status```: Método para determinar el estado de un determinado hilo.

    >```Thread#value```: Método que retorna el valor de la última sentencia ejecutada por el Thread (es una variante de ```Thread#join```).

    >```Thread.list```: Método que retorna una lista de todos los objetos Thread que están en ejecución o bloqueados.

    >```Thread#priority=```: Método para establecer la prioridad de un hilo.

    >```Thread.stop```: Método que bloquea el hilo actual.

    >```Thread#run```: Método que permite la ejecución de un hilo.

    >```Thread.pass```: Método que permite cambiar el hilo en ejecución a otro hilo.

4. ¿Que se obtiene como salida? Describa el comportamiento del programa. :star:
    ```ruby
    print Thread.main
    print "\n"
    t1 = Thread.new {sleep 100}
    Thread.list.each {|thr| p thr }
    print "Current thread = " + Thread.current.to_s
    print "\n"
    t2 = Thread.new {sleep 100}
    Thread.list.each {|thr| p thr }
    print Thread.current
    print "\n"
    Thread.kill(t1)
    Thread.pass # pass execution to t2 now
    t3 = Thread.new do
      sleep 20
      Thread.exit # exit the thread
    end
    Thread.kill(t2) # now kill t2
    Thread.list.each {|thr| print thr }
    # now exit the main thread (killing any others)
    Thread.exit
    ```
    > **SALIDA**
    ```
    (No hace falta poner nada literal)
    ```

    >Primero se muestra el Thread (hilo) principal. Seguidamente, se crea un hilo t1 y se muestran todos los hilos existentes juntos con el hilo actual. A continuación se crea un hilo t2 y se vuelven a mostrar todos los hilos existentes junto con el hilo actual. Luego se elimina el hilo t1 y se pasa como hilo en ejecución al hilo t2. Se crea otro hilo t3 y después termina el hilo t2.Por último se listan todos los hilos existentes y se termina el hilo principal, terminando con cualquier otro hilo que estuviera presente.

5. Describa el tipo de acceso que tienen los Threads a los distintos tipos de variables en Ruby.:star:

    >Los hilos pueden acceder a cualquier tipo de variable que esté bajo su ambiente a la hora de su creación.

6. ¿Como se tratan las variables locales a los Threads en Ruby?:star:

    >Las variables locales que contiene el bloque recibido por el hilo son locales al hilo, y no pueden ser compartidas. La clase Thread proporciona una utilidad que permite crear variables locales al thread y ser accedidas por el nombre. Simplemente se trata el objeto Thread como si fuera un Hash, escribiendo elementos utilizando ```[]=``` y leyendo con ```[]```. La sintaxis es ```hilo[“nombre de la variable”] = valor```.

7. ¿Qué se obtiene como salida? Describa el comportamiento del programa. :star:
    ```ruby
    count = 0
    threads = []
    10.times do |i|
      threads[i] = Thread.new do
        sleep(rand(0.1))
        Thread.current["mycount"] = count
        count += 1
      end
    end
    threads.each {|t| t.join; print t["mycount"], ", "}
    puts "count = #{count}"
    ```
    > **EJEMPLO DE SALIDA**
    ```
    7, 0, 8, 6, 5, 4, 1, 9, 3, 2, count = 10
    ```
    >En este programa, primero se crea un array de threads llamado ```threads```. Después se crean diez hilos, con un tiempo de espera aleatorio para cada uno antes de que cada hilo almacene el valor actual de la variable ```count``` en una variable de hilo local con la clave ```mycount``` y, después de almacenar la variable, la incrementa en una unidad. Por último, para cada hilo se invoca al método ```join``` para bloquear el hilo principal hasta que se terminen de ejecutar todos los hilos y se imprime la variable local ```mycount``` de cada hilo.

    >En este trozo de código hay una condición de carrera debido a que es posible que un hilo establezca el valor de su variable local ```mycount``` al valor de ```count``` pero, antes de que tenga oportunidad de incrementar la variable ```count```, el hilo se deja de ejecutar y otro hilo reutiliza el mismo valor de ```count```. Esto se arregla sincronizando el acceso a recursos compartidos (como la variable ```count```).

8. ¿Que es una condición de carrera (race condition)? :star:

    >Es la situación en la que dos o más trozos de código (hilos) intentan acceder a algún tipo de recurso compartido al mismo tiempo, y el resultado cambia dependiendo del orden en el que los hilos acceden a dicho recurso (un dato, por ejemplo). Esto puede dar lugar a una corrupción de datos, ya que se puede obtener un valor inesperado al haber sido modificado el dato compartido por otro proceso.

9. ¿Que se obtiene como salida? Describa el comportamiento del programa. :star:
    ```ruby
    def inc(n)
      n + 1
    end
    sum = 0
    threads = (1..10).map do
      Thread.new do
        10_000.times do
            sum = inc(sum)
          end
        end
    end
    threads.each(&:join)
    print sum
```
    > **EJEMPLO DE SALIDA**
    ```
    17735
    ```
    >En este programa primero se crea un método ```inc``` que incrementa la variable que le pasan en uno y retorna la variable incrementada. Después se crean 10 hilos y cada uno de ellos incrementa la variable compartida ```sum``` 10,000 veces. Por último, cada uno de los hilos invoca ```join``` para que el hilo principal les deje terminar.

    >Cuando todos los hilos terminan (debido a que se acaban las 10,000 iteraciones de cada uno), el valor final de la variable ```sum``` es considerablemente menor que 100,000. Por lo tanto, se presenta una condición de carrera puesto que no hay ningún tipo de sincronización. Por ejemplo, en un hilo se llama a ```inc```, pasándole el valor actual de ```sum``` (por ejemplo, 99) y retorna el valor 100 (valor actual de ```sum```). No obstante, lo que puede ocurrir es que este hilo, antes de asignar el nuevo valor a ```sum```, otro hilo sustituya al hilo actual y, por tanto, también pase el valor 99 a ```inc```, devuelva el valor 100 y se lo asigne a ```sum```. Si el hilo anterior se vuelve a ejecutar, también asignará el valor 100 a ```sum``` de nuevo. Por tanto, se ha perdido información debido a esta condición de carrera.

10. ¿Que es la exclusión mutua (mutual exclusion)?:star:

    >Es un mecanismo que permite evitar las condiciones de carrera en secciones críticas cuando varios hilos acceden a datos en memoria concurrentemente.

11. ¿Para que se utilizan los objetos de la clase Mutex en Ruby? :star:

    >Son semáforos que se utilizan para sincronizar el acceso de varios hilos a un recurso compartido. Se utilizan para crear secciones sincronizadas (áreas de código donde solo un hilo puede entrar a la vez).

12. ¿Que se obtiene como salida? Describa el comportamiento del programa. :star:
    ```ruby
    def inc(n)
      n + 1
    end

    mutex = Mutex.new
    sum = 0
    threads = (1..10).map do
      Thread.new do
        10_000.times do
          mutex.synchronize do
            sum = inc(sum)
          end
        end
      end
    end
    threads.each(&:join)
    print sum
    ```
    > **SALIDA**
    ```
    100000
    ```
    >En este programa primero se crea un método ```inc``` que incrementa la variable que le pasan en uno y retorna la variable incrementada y después se crea un objeto Mutex de nombre ```mutex```. A continuación se crean 10 hilos y cada uno de ellos incrementa la variable compartida ```sum``` 10,000 veces. Para controlar el incremento de la variable, el objeto Mutex ```mutex``` invoca al método ```synchronize```, que bloquea el mutex, ejecuta el código dentro del bloque y después desbloquea el mutex.Por último, cada uno de los hilos invoca ```join``` para que el hilo principal les deje terminar.

    >Mediante el uso del objeto Mutex se consigue evitar una condición de carrera puesto que con este mutex se controla el acceso a un recurso y lo bloquea cuando un hilo quiere hacer uso de este recurso. De esta manera, si nadie más lo ha bloqueado, el hilo sigue ejecutándose. Si alguien más ha bloqueado ya ese mutex en particular, el hilo se queda en suspensión hasta que se desbloquee.

13. ¿Que es un abrazo mortal (deadlock)? :star:
    >Es el bloqueo permanente de un conjunto de procesos o hilos de ejecución en un sistema concurrente que compiten por recursos del sistema.

14. ¿Qué mecanismo proporciona Ruby para evitar los deadlock? :star:

    >Las variables de condición. Esto permite la comunicación mediante eventos entre dos hilos y esta comunicación se emplea para establecer el orden en el que los threads ejecutan su código. La función principal de las variables de condición es secuencializar la ejecución de los hilos.

15. ¿Que se obtiene como salida? Describa el comportamiento del programa. :star:
      ```ruby
      mutex = Mutex.new
      cv = ConditionVariable.new
      a = Thread.new {
        mutex.synchronize {
          print "A: Esta en una region critica, esperando por cv\n"
          cv.wait(mutex)
          print "A: Esta en la region critica de nuevo!. Sigue\n"
        }
      }
      print "En medio\n"
      b = Thread.new {
        mutex.synchronize {
          puts "B: Esta en la region critica pero tiene a cv"
          cv.signal
          puts "B: Esta en la region critica, Saliendo"
        }
      }
      a.join
      b.join
    ```
    > **SALIDA**
    ```
    En medio
    A: Esta en una region critica, esperando por cv
    B: Esta en la region critica pero tiene a cv
    B: Esta en la region critica, Saliendo
    A: Esta en la region critica de nuevo!. Sigue
    ```
    >En este programa se crean dos hilos sincronizados por el mismo mutex. Para evitar un deadlock se utiliza una variable de condición para la comunicación entre dos hilos. De esta forma, el hilo ```a``` se ejecuta primero e imprime el primer mensaje y, a continuación, la  variable de condición invoca al método ```wait```, de tal forma que libera el bloqueo del mutex para que otro hilo se ejecute antes que él y espera a una señal de éste para que se pueda volver a ejecutar. De esta manera, el hilo ```b``` puede ejecutarse e imprime su primer mensaje. Cuando el hilo ```b``` ha terminado de utilizar el recurso, la variable de condición invoca al método ```signal```, que envía una señal y despierta al primer hilo que estaba esperando por el bloqueo. El hilo ```b``` imprime el segundo mensaje antes de acabar y, a continuación, el hilo ```a``` imprime su segundo mensaje.
