# Problemas de LPP

Ejercicios tipo que caen en examenes de LPP

# Guia

Las respuestas van como ~~**Blockquote**~~ texto normal.

> Esto es una respuesta a un ejercicio

Respetar identación para que el parseador de Markdown continúe con el indice numérico de la pregunta

Las respuestas con :star: son respuestas verificadas.
Las respuestas con :sos: son respuestas sin verificar.

# Recomendaciones

Usar 4 espacios como tabulación.

# Documentación

* [GitLab Flavored Markdown](https://docs.gitlab.com/ce/user/markdown.html)
* [Emoji Cheat Sheet](http://emoji.codes/)
