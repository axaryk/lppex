# Lenguajes y Paradigmas de Programación
## Hoja de Problemas
### Tema 3

<!-- Escuela Superior de Ingeniería Informática
Lenguajes y Sistemas Informáticos
Universidad de La Laguna -->

1. ¿Qué es una clase?

    > Una clase es un molde que se emplea para crear objetos instancia que cumplen las características de la clase en cuestión. Dichos objetos poseen métodos y atributos que pueden ser del propio objeto en sí, o de la clase.

2. ¿Qué es un objeto?

    > Un objeto es una instancia de una clase, un caso particular de esa clase.

3. ¿Qué es una variable local?

    > Una variable local es una variable que sólo puede accederse desde la función o bloque de instrucciones en donde se declara.

4. ¿Qué es una variable de instancia?

    > Es una variable que empieza con @ y cuyo ambito está limitado al objeto al que referencia self. Dicha variable no es explícitamente accesible desde el exterior del objeto.

5. ¿Qué es una variable global?

    > Es una variable que se define por fuera de cualquier método y  cuyo ámbito abarca cualquier punto de todo el programa. Empiezan con $.

6. ¿Qué es una variable de clase?

    > Es una variable que empieza con @@ y que es propia a la clase que la contiene y no a las instancias de esta, esto es, todos los objetos que se creen de la clase compartirán dicha variable.

7. ¿Qué es un método de acceso (getter)?

    > Es un método que permite acceder a los valores de las variables de instancia de un objeto. En Ruby se suele utilizar el método por defecto attr_reader seguido del símbolo de la variable.

8. ¿Qué es un método de asignación de valores (setter)?

    > Es un método que permite modificar el valor de las variables de instancia de un objeto. En Ruby se suele utilizar el método por defecto attr_writer seguido del símbolo de la variable.

9. ¿Cuál es la visibilidad del método initialize?:star:

    > La visibilidad del método initialize es privada; sólo es invocado cuando el método de clase new es llamado con parámetros(cuando un objeto es creado a partir de esa clase). Al crear un objeto de una clase con ​ new, el ​ método de clase ​ new en la clase Class es ejecutado por defecto, que invoca al método ​ allocate para alojar memoria para el objeto, y después llama al método ​ initialize del objeto a crear para inicializar el objeto.

10. El valor retornado por initialize es usado para la construcción del objeto. ¿Verdadero o falso?

    > Falso, ya que el método initialize solo se usa para inicializar el objeto, no se utiliza para construir el objeto. El valor retornado es nil, que indica únicamente que el objeto se ha construido correctamente, pero no interviene en su construcción. El encargado de reservar la memoria para un objeto es allocate.

11. Considere el siguiente código Ruby:

    ```ruby
    class AClass
        @x=4
        @y=9
        def initialize(x,y)
            @x, @y = x, y
        end
    end
    ```

    * ¿Qué tipo de variable son las de las líneas 2 y 3? ¿Cuál es su visibilidad? Explique su respuesta. :star:

        > Son variables de instancia de la clase y su ámbito está limitado al objeto que representa la clase que incluye este tipo de variables. Estas variables no son heredadas por subclases de esa clase. Son visibles desde la clase.

    * ¿Qué tipo de variable son las de la línea 5? ¿Cuál es su visibilidad? Explique su respuesta.:star:

        > Son variables de instancia del objeto instanciado de la clase y son visibles desde el objeto de la clase.Debido a que cada objeto tiene su propio conjunto de variables de instancia, cada objeto puede tener su propio estado.

12. Señale el error de concepto en la codificación de la siguente clase Ruby:

    ```ruby
    class MyClass
        def initialize(b)
            @b = b
        end
        def to_s
            puts @b
        end
    end
    ```

    >  El método ​ to_s tiene que devolver una cadena, pero ​```puts``` es una llamada a una función (la línea del ​ puts no es una cadena) y devuelve nil.

13. Considere el siguiente código Ruby: :star:

    ```ruby
    class AClass
        attr_accessor :n
        #...
        def initialize(n)
            @n = n
        end
        #...
        def square
    9        n = n * n
        end
        #...
    end
    ```

    En los accesos a "n" a la derecha de la igualdad en la línea 9

        * ¿A qué tipo de variable se está accediendo?:star:

            > Se accede a una variable de instancia de un objeto de la clase AClass mediante el getter n.

        * ¿Y en el acceso a la izquierda de la igualdad? :star:

            > Se modifica la variable de instancia del objeto de la clase AClass mediante el setter n.

        * ¿attr_accessor es un método de instancia o de clase? :sos:

            > Es un método de instancia.

        * ¿En qué clase está definido attr_accessor?:star:

            > El método ​ attr_accessor pertenece a la clase Module. Todas las clases son módulos (la clase Class es una subclase de Module), así que se puede invocar este método dentro de la definición de cualquier clase.

14. Considere la creación de una clase Ruby para representar números fraccionarios. ¿Cómo se denominan a los métodos necesarios para definir las siguientes operaciones? ¿Cuántos argumentos reciben? :star:

    * la suma, la resta, la multiplicación, la división,

        ```ruby
        class Fraction
            attr_accessor :n, :d
            def initialize(n,d)
                @n = n
                @d = d
            end

            def +(other_fraction)

            end

            def -(other_fraction)

            end

            def *(other_fraction)

            end

            def /(other_fraction)

            end

        end
        ```

    * el opuesto,

        ```ruby
        def -@
        end
        ```

    * la indexación

        ```ruby

        def [](i)

        ```

    * la asignación a un elemento del objeto

        ```ruby
        def []=(i,value)
        ```
    > Se denominan métodos de instancia y todos ellos reciben un argumento.

15. ¿Qué es el polimorfismo?:star:

    > Se define como la capacidad que posee un método o un objeto de responder de formas distintas según las entradas.Es la propiedad por la que es posible enviar mensajes (llamar a métodos) con el mismo nombre a objetos de clases distintas.  Por ejemplo, un método suma() puede sumar enteros, complejos, arrays, conjuntos...

16. ¿Cuál es la diferencia entre tipo y clase? :star:

    > El ​ tipo​ de un objeto es el conjunto de métodos a los que puede responder; es el conjunto de conductas que caracterizan al objeto. La ​clase​ es un patrón para construir el objeto, pero el comportamiento del objeto se puede alejar del proporcionado por la clase. De esta forma, podemos tener distintas clases que
    pueden corresponder al mismo tipo, y así podemos hablar de características Ruby como el conocido tipeado pato.

17. ¿Qué ventajas e inconvenientes se tienen si en la escritura de un método metodo(x,y) se usa respond_to? para comprobar que los argumentos x e y responden a los métodos llamados en el cuerpo del método metodo? :star:

    > Una de las ventajas es que permite el tipeado pato, de tal forma que se tiene en cuenta si en el comportamiento del objeto se incluyen esos métodos, dejando más de lado la clase de la que procede el objeto.El inconveniente que tiene es que sólo comprueba el nombre del método, no los argumentos que tiene ese método
    (no comprueba sus tipos).

18. ¿Qué ventajas e inconvenientes se tienen si en la escritura de un método metodo(x,y) se usa is_a? para comprobar que los argumentos x e y pertenecen a las clases esperadas por el método metodo? :star:

    > Al comprobar si los argumentos x e y pertenecen a una determinada clase o herencia, no permite el tipeado pato. No obstante, tienes un mayor control sobre las clases de objetos que se permiten pasar por parámetro y podemos actuar de distinta manera dependiendo de si el objeto posee una linea se sucesión u otra.

19. En Ruby ¿el conocimiento de la clase obj.class del objeto obj caracteriza la conducta del objeto? :star:

    > No. Esto es debido a que obj.class retorna a qué clase pertenece el objeto. No obstante, no sabemos cómo se comporta un objeto instanciado de esa clase; para descubrir su comportamiento, debemos mirar el código de sus métodos de instancia.

20. ¿Qué clase de objeto crea la llamada Fraction = Struct.new(:num, :denom)? :star:

    >Crea un objeto de la clase 'Class' llamado Fraction con dos variables de instancia @num y @denom, y sus correspondientes getter y setter para cada una. Con Struct se crea una clase modificable (mutable).

21. ¿Cómo se puede impedir que se invoque a los métodos num= y denom= de la clase Fraction = Struct.new(:num, :denom)?

    > Se puede impedir haciendo que la clase creada con Struct sea inmutable. Para eso se hace uso del método ​ undef; se abre la clase creada con Struct y se deshabilita los métodos predefinidos ​ num= y ​denom=.

    ```ruby
    class Fraction
        undef num=
        undef denom=
    end
    ```

22. ¿Qué es una variable de clase? :star:

    > Es una variable que empieza con @@ y que  es una variable que es visible y compartida por todos los objetos instanciados de una misma clase y también por la propia definición de clase.

23. ¿En qué forma se define en Ruby un método de clase?

    > Un método de clase se puede definir de tres formas distintas:

      >● Definiendo el método de clase anteponiendo el nombre de la clase explícitamente.

      >● Definiendo el método de clase anteponiendo self ( nombrando a la clase implícitamente, no se repite el nombre de la clase).

      ```ruby
      class AClass
          def self.metodo_de_clase()
          end
      end
      ```
      >● Abriendo la clase para definir múltiples métodos de clase.

      ```ruby
      class << AClass
        def adios
          puts "adios"
        end
      end
      ```
24. ¿Es posible definir una constante de la clase MyClass antes de la definición del método initialize? :star:

    > No se puede debido a que las constantes crean instancias de la clase y no podemos definirlas antes de tener definida la versión correcta del método initialize.

25. ¿Es posible definir constantes de una clase desde fuera de la misma?:star:

    > Si. Fuera de la clase o módulo, se pueden usar mediante el operador :: precedido de una palabra que indique el módulo o clase apropiados.

26. ¿A qué clase de variable n permite acceder la declaración de la línea 3?:star:

    ```ruby
    class Tutu
        class << self
            attr_accessor :n
        end
    end
    ```

    > A una variable instancia de clase.

27. ¿Qué se entiende por herencia?:star:

    > La herencia es un mecanismo de la programación orientada a objetos que permite crear una clase que sea un refinamiento o especialización de otra clase. Esta clase se llama subclase del original, y el original es una superclase de la subclase.

    >Por otra parte, la clase hija tiene la capacidad de ampliar y modificar el comportamiento de los métodos y atributos heredados.

28. Enumere los tipos de herencia y descríbalos brevemente.

    > **Herencia Simple**: Una clase sólo puede tener como clase ancestra una única clase madre (sólo puede heredar de una clase base y de ninguna otra).

    > **Herencia Multiple**: Una clase puede tener varias clases madres.

29. ¿Qué tipo de herencia proporciona Ruby?

    > Herencia simple.

30. ¿Qué se entiende por invalidación de un método (overriden)?

    > Se entiende por ​ invalidación de un método la posibilidad de reemplazar la conducta de un método definido en la superclase. Es una característica de lenguaje que permite que una subclase proporcione una implementación específica de un método que ya es proporcionado por su superclase.

31. ¿Qué puede ocurrir si en una subclase se escribe un método con nombre "intimo" igual al de un método privado "intimo" de la superclase? ¿Qué ocurre si el método toto de la superclase llama a intimo? :star:

    > Debido a que ​ intimo en la superclase es privado, la subclase no va a heredar el método,por lo que si se define el método ​ intimo en la subclase, no va a sobrescribir el método del padre.

    >Si el método ​ toto de la superclase llama a ​ intimo, se invocará el método privado de la superclase.

    > Ejemplo(intimo)

    ```ruby
    class SuperClase
        def intimo
            "Soy de la clase SuperClase"
        end
    end

    class SubClase < SuperClase
        def intimo
            "Soy de la clase SubClase"
        end
    end

    superi = SuperClase.new
    superi.intimo

    # => "Soy de la clase SuperClase"

    subi = SubClase.new
    subi.intimo

    # => "Soy de la clase SubClase"

    binding.pry
    ```

    > Ejemplo (toto)

    ```ruby
    class SuperClase
        def toto
            intimo
        end
        def intimo
            "Soy de la clase SuperClase"
        end
    end

    class SubClase < SuperClase
        def intimo
            "Soy de la clase SubClase"
        end
    end

    superi = SuperClase.new
    superi.toto

    # => "Soy de la clase SuperClase"

    binding.pry
    ```
32. ¿Qué ocurre cuando se llama a super sin argumentos? :star:

    > Cuando se invoca a ​ super sin argumentos, todos los argumentos que se pasaron al método actual se pasan al método de superclase.

33. ¿Cómo se puede llamar a super sin argumentos? :star:

    ```ruby
    super
    super()
    ```

    > Mirar en este [enlace](http://rubytutorial.wikidot.com/herencia).

34. Describa la salida de estos dos programas Ruby: :star:

    ```ruby
    class A
        @@value = 1
        def A.value
            @@value
        end
    end

    puts A.value

    class B < A
        @@value = 2
    end

    puts A.value
    puts B.value

    class C < A; @@value = 3; end

    puts B.value
    puts C.value

    puts A.class_variables.inspect
    puts B.class_variables.inspect
    puts C.class_variables.inspect
    ```
    > **SALIDA**
    ```
     1
     2
     2
     3
     3
     ["@@value"]
     []
     []
    ```
    >En el primer programa el valor A.value es 1, ya que accede a esa variable de clase y su valor por defecto es 1. Cuando se crea una subclase B, que hereda de la clase A, el valor de la variable de clase ahora es 2 y, por
    consiguiente, tanto el valor de A.value como el de B.value es 2, ya que las variables de clase se heredan y los dos valores apuntan a la misma zona de memoria.  De esta manera, la subclase C, al heredar de la clase A, cambia el valor de la variable de clase a 3, por lo que ahora A.value, B.value y C.value apuntan a la
    misma zona de memoria y, por tanto, valen lo mismo. En cuanto a los inspect, la clase A tiene la variable de clase value, pero en las clases B y C no aparece esta variable puesto que el método ```class_variables``` tiene puesto a false las variables de clase heredadas, pero en realidad si la tienen como variable de
    clase.

    ```ruby
    class A
        @value = 1
        def self.value
            @value
        end
    end

    puts A.value

    class B < A
        @value = 2
    end

    puts A.value
    puts B.value

    class C < A; @value = 3; end

    puts B.value
    puts C.value
    ```
    > **SALIDA**
    ```
     1
     1
     2
     2
     3
    ```

    > En el segundo programa, ​value ya no es una variable de clase, sino una variable de instancia de la clase, de tal forma que este tipo de variables no son heredadas por las subclases. Cada una de estas variables es propia de la clase donde se encuentra definida. De esta forma, los valores de ​ value en A, B y C son
    respectivamente 1,2 y 3.

35. Suponga que la clase B hereda de A un método tutu que usa la constante C definida en A. Si en la clase B se define C, ¿Qué definición de C usará tutu, la de A o la de B? :star:

    >Usará la constante de A ya que, al no encontrar el método en la subclase, sube en la herencia y va al padre, que si tiene el método. Como ya subió en la herencia, va a utilizar la constante de A y no puede acceder a B.

36. ¿Qué es la encapsulación?

    > La encapsulación es el mecanismo que oculta el acceso a las estructuras de datos de un objeto (atributos). Es un mecanismo que consiste en organizar datos y métodos de una estructura, conciliando el modo en que el objeto se implementa; es decir, evitando el acceso a datos por cualquier otro medio distinto a los especificados y de tal forma que la representación interna de un objeto está oculta de la vista fuera de la definición del objeto . Por lo tanto, la encapsulación garantiza la integridad de los datos que contiene un objeto.

37. ¿Cuál es la visibilidad por defecto de un método?

    > Publica.

38. ¿Cuál es la visibilidad por defecto de un método que ha sido definido fuera de cualquier clase (por ejemplo en un script)? :sos:

    >Los métodos declarados fuera de cualquier definición de clase son declarados como métodos de instancia privados de la clase Object.

    >http://stackoverflow.com/questions/27767534/why-are-global-methods-allowed-to-be-defined-outside-of-a-class-in-ruby

39. Los métodos privados no pueden ser llamados desde una subclase, ¿cierto o falso? :star:

    > Cierto, ya que los métodos privados solo se pueden invocar desde el contexto del objeto actual. Para que pudieran acceder las subclases, habría que declararlos como protected.

40. Dentro de una clase y fuera de un método self, ¿a qué objeto hace referencia self?

    > Hace referencia a la clase dentro de la que se encuentra.

41. ¿Cómo se denomina la superclase de la clase Class?:star:

    > Module

42. La palabra reservada private seguida del nombre de un método permite restringir el acceso a dicho método. ¿Qué es falso en la afirmación anterior?:star:

    > ```private``` no es una palabra reservada, es un método. private, protected y public son métodos de instancia de la clase Module.

43. ¿Qué es un módulo?

    > Un módulo es similar a una clase, ya que contiene una colección de métodos, constantes, variables de clase y otros módulos y definiciones.
    > A diferencia de las clases, no se pueden crear instancias ni subclases derivadas de los módulos.
    > Tienen dos funciones:
        * Como librerías para ampliar una clase con nuevos comportamientos (mixins).
        * Como espacios de nombres.

44. ¿Qué es un espacio de nombres (namespace)?

    > Un espacio de nombres proporciona un contenedor que permite organizar un conjunto de clases, métodos y constantes de forma lógica y evitar conflictos con otros métodos, clases y constantes con el mismo nombre que han sido escritas por otra persona.

45. ¿Qué es un (mix-in)?

    > Un mix-in es un módulo en Ruby que se utiliza para ampliar una clase con nuevos comportamientos, de tal forma que los métodos del módulo se incluyen en los métodos de la clase. Permite simular una herencia múltiple, pero no es una herencia propiamente dicha.

46. ¿Qué es Enumerable?

    > El módulo mix-in Enumerable  proporciona clases de colección con varios métodos de recorrido y búsqueda, y con la capacidad de ordenar. La clase debe proporcionar un método ```each```, lo que produce miembros sucesivos de la colección.

47. ¿Qué es Comparable?

    > Es un módulo que provee métodos de comparación; se utiliza por clases cuyos objetos pueden ser ordenados. Para emplearlo, la clase debe definir el operador <=>, que compara el receptor con otro objeto, devolviendo -1, 0 o +1 dependiendo de si el receptor es menor que, igual o mayor que el otro objeto.

48. ¿Qué operador se ha de definir para que a partir de él se puedan comparar elementos de la clase?

    > El operador <=>

49. ¿Qué módulo se debe incluir para poder ordenar objetos?:sos:

    >El módulo Enumerable pero, para que se pueda utilizar la ordenación sobre los elementos, éstos deben ser comparables.

50. ¿Qué diferencias hay entre los siguientes predicados?
    * ==

        >  Es el operador más utilizado para comprobar si dos objetos tienen el mismo valor. En la clase Object, es un sinónimo de equal? y testea si dos objetos son idénticos en valor. La mayoría de las clases redefinen este operador.Ej.: (1==1.0) True

    * eql?

        > Utilizado para comprobar si tanto un objeto como el otro tienen el mismo valor y el mismo tipo ( se utiliza estrictamente para que no haya conversión de tipo). Ej.: (1.eql? 1.0) False

    * equal?

        > El metodo equal? , por convención, las subclases nunca invalidan este método. Es usado para comprobar si se refieren al mismo objeto ( si tienen el mismo ID) (a.equal?(b) si a es el mismo objeto que b).

    * ===
        > Este operador es comúnmente llamado el operador “igualdad case” y es utilizado para comprobar si el valor objetivo de una sentencia case concuerda con alguna cláusula when de esa sentencia.

    * =~

        > Es el operador de concordancia y su función es  comparar una expresión regular con una determinada cadena y comprobar  si ese patrón se encuentra dentro de la cadena. Si encuentra algo, devuelve la cadena o subcadena encontrada. En caso contrario, devuelve nil.

51. ¿Cuál es el resultado? :star::

    ```ruby
    (1..10) === 5
    ```
    => true

    ```ruby
    /\d+/ === "123"
    ```
    => true

    ```ruby
    String === "s"
    ```
    => true

    ```ruby
    :s === "s"
    ```
    => true

52. ¿Cuál es la diferencia entre obj.nil? y obj == nil? :star:

    > obj.nil? es un método de la clase Object que retorna si el objeto invocante es o no nulo.
    > obj == nil el operador == realiza una comparación y retorna true si el objeto obj es nil, puesto que el objeto nil es nil.

53. ¿Cómo se puede permitir que los objetos de la clase Fraction = Struct.new(:num, :denom sean comparables?

    > Haciendo un include del módulo Comparable (abriendo la clase Fraction) y definiendo el método <=> para la clase Fraction.

54. ¿Qué predicado es usado por Ruby para comprobar la igualdad entre claves de un hash?

    > El predicado eql?

55. ¿Cómo se puede conseguir qué el producto de un número por un objeto de una clase que se está definiendo funcione? Por ejemplo: 4 * obj :sos:

    > Para que pueda funcionar, se llama implícitamente al método coerce, que es un método de conversión que devuelve un array de dos posiciones; la primera contiene el objeto que va a recibir (invocar) el método en cuestión y la segunda se corresponde con el argumento de ese método. En la operación 4*obj, 4 es
    un Fixnum, pero no sabe cómo multiplicarse con el objeto obj. Por consiguiente, el Fixnum invoca el método coerce de su argumento, en este caso obj (la clase de la que instancia el objeto obj debe implementar su propio método coerce).

    ```ruby
    def coerce (other)
        [self, other]
    end
    ```

56. protected es un método de instancia de la clase Module ¿Verdadero o falso?

    > Verdadero.

57. ¿Disponen los elementos de la clase Module de un método new? :star:

    >No ya que , a diferencia de una clase, un módulo no puede ser instanciado ni tampoco ser heredado.

58. ¿Qué es una clase abstracta?:sos:

    > Una clase abstracta, en general, es una clase que no se puede instanciar y se usa únicamente para definir subclases. Sin embargo, en Ruby, no hay clases  abstractas, sino que directamente se utiliza la jerarquía de clases para realizar esta función. En Ruby, se entiende por clase abstracta aquella que invoca
    métodos que está previsto que una subclase las implemente.

59. Cómo se define una clase concreta?

    >Una clase concreta es una clase que extiende una clase abstracta y que define todos los métodos “abstractos” de sus ancestros.

60. ¿Cómo se denomina la clase a la que pertenecen las clases de Ruby? Dibuje la jerarquía de clases a la que pertenece. :sos:

    >La clase Class. (Hacia arriba) Class ->  Module -> Object -> BasicObject

61. ¿Qué se obtiene como salida? :sos:

    ```ruby
    class MyClass
    end
    ```
    => Class

    ```ruby
    MyClass.class
    ```
    => Class

    ```ruby
    String.class
    ```
    => Class

    ```ruby
    > Object.class
    ```
    => Class

    ```ruby
    > Class.class
    ```
    => Class

62. ¿Qué se obtiene como salida? Describa el comportamiento del programa.:star:

    ```ruby
    def show (a_class)
    if (a_class != nil) then
            puts "#{a_class}:: es hija de = #{a_class.superclass}"
                show(a_class.superclass)
        end
    end
    show(Fixnum)
    ```
    > **SALIDA**
    ```
    Fixnum:: es hija de = Integer
    Integer:: es hija de = Numeric
    Numeric:: es hija de = Object
    Object:: es hija de = BasicObject
    BasicObject:: es hija de =
    ```
    >Lo que se obtiene a la salida es la jerarquía de la clase Fixnum. El programa consiste en un método show, al que se le pasa el nombre de una clase existente  y, si la clase no es nil, se muestra el nombre de la clase y al lado el nombre de su clase padre (con superclass). A continuación, realiza una llamada recursiva
    al mismo método pero pasándole como parámetro la superclase de la clase que estábamos imprimiendo. De esta manera, el programa va imprimiendo toda la jerarquía hasta que se llega a BasicObject, que es la última clase de la jerarquía y por encima de él es nil.

63. ¿Qué es un método singleton?:sos:

    >Un método singleton es un método x que se añade a un objeto obj específico. Sólo está disponible en el objeto en el que lo defines.

64. ¿Cuál es la sintaxis Ruby para crear un método singleton?:sos:

    >Para definir un método singleton, primero hay que nombrar al objeto donde se quiere guardar el método seguido de un punto y del nombre del método.

65. ¿Qué se obtiene como salida? Describa el comportamiento del programa.:star:

    ```ruby
    class AClass
        def m
            ’method m’
        end
    end

    obj = AClass.new

    def obj.s1
        ’method s1’
    end

    class << obj
        def s2
            ’method s2’
        end
    end

    puts obj.m
    puts obj.s1
    puts obj.s2
    ```
    > **SALIDA**
    ```
    method m
    method s1
    method s2
    ```
    >Primero se declara la clase AClass, donde se define el método m que devuelve una cadena con el nombre de este método. A continuación se crea un objeto obj y después se define un método singleton s1 para ese objeto.Posteriormente,  se abre la clase singleton del objeto para añadirle otro método singleton llamando s2, que devuelve una cadena con el nombre de este método. Por último, se imprimen los tres métodos. A la hora de mostrar lo que devuelven estos tres métodos, no da error, ya que el método m es un método de instancia de la clase de la que procede el objeto obj y los otros dos métodos se definen únicamente para ese objeto.

66. ¿Qué es la eigenclass?

    >Es  una clase anónima asociada con el objeto. Contiene los métodos singleton de ese objeto.

67. Sea x una variable que contiene un objeto de la clase Class. ¿Con qué otro nombre se conoce a los métodos singleton del objeto x?:star:

    >Métodos de clase.

68. ¿En qué clase se alojan los métodos singleton de x?:star:

    >En su eigenclass.

69. ¿Qué diferencia hay entre la eigenclass de un objeto de la clase Class y la de un objeto ordinario?:star:

    >La eigenclass de un objeto ordinario no tiene superclase, mientras que la eigenclass de un objeto de la clase Class si.

70. ¿Qué queda en z?

    ```ruby
    z = class << String; self end
    ```
    >La eigenclass de la clase String.

71. ¿Qué ocurre si en una clase C se incluyen dos módulos M1 y M2 (en ese orden) en los que existen métodos con el mismo nombre a_method?:star:

    > Cuando invoque un objeto instanciado de la clase C ese método, va a llamar al método incluido en el módulo M2 , ya que se busca primero en el módulo que aparezca en último lugar.

72. ¿Qué ocurre si en una clase C se incluye un módulo M en el que existe un método a_method con el mismo nombre que un método a_method que ya existe en la clase C?:star:

    > Si en la clase existe un método a_method ese será encontrado primero, ya que se busca primero en la clase antes que en los mix-in introducidos en la clase.

73. Considere la expresión obj.m donde se llama al método m con receptor el objeto obj. ¿En qué clase busca Ruby primero a m? :star:

    > En la Eigenclass del objeto obj.

74. Describa el algoritmo de búsqueda de Ruby para el método my_method()::star:

    ```ruby
    class MyClass
        def my_method; ’my_method()’; end
    end

    class MysubClass < MyClass
    end
    obj = MySubclass.new
    obj.my_method()
    ```
    >En este programa se está haciendo una búsqueda de un método de instancia. Por tanto, primero se busca el método en la eigenclass del objeto obj(self se coloca en esta eigenclass). Como no está ahí, self se posiciona en la clase de este objeto. Al no encontrarlo,se busca en los módulos incluidos en esa  clase, pero no hay ninguno, así que se sube al padre de esta clase y ahí lo encuentra.

75. Describa el algoritmo de búsqueda de Ruby para el método my_method()::star:

    ```ruby
    module M
        def my_method; ’M#my_method()’; end
    end
    class C
        include M
    end
    class D < C ; end
    D.new.my_method()
    ```
    >Primero busca en la clase D, pero no está definida ahí. Luego se busca en los módulos incluidos en D, pero no hay y sube a la clase padre (la clase C) y no lo encuentra, pero más adelante mira en el módulo M incluido en la clase C, donde sí se encuentra el método.

76. Considere el siguiente código Ruby::star:

    ```ruby
    def Integer.a_method(x)
        x.to_i
    end
    n = Fixnum.a_method("5")
    ```
    Describa el algoritmo de búsqueda de la invocación de la línea 4.

    >Primero se busca en la eigenclass de Fixnum, pero ahí no lo encuentra. A continuación busca en la clase eigenclass padre(clase Integer) y ahí encuentra el método.

77. ¿Qué es un método singleton de una clase?

    >Es un método de clase de esa clase; es un método que sólo puede llamar el objeto que representa la clase.

78. ¿Qué se está declarando?

    ```ruby
    class << AClass
        def a_method
            ’a_method’
        end
    end
    ```
    >Se está declarando un método singleton a_method dentro de la clase AClass.

79. ¿Qué tipo de ámbito se utiliza para la búsqueda de constantes en Ruby?:sos:

    >Se busca por la definición apropiada de la constante. Ámbito léxico.

80. ¿Qué se obtiene como salida? Describa el comportamiento del programa. ¿Qué contiene la variable
search?:star:

    ```ruby
    module Kernel
        A=B=C=D=E=F="defined in Kernel"
    end
    #Top level or global constant defined in Object
    A=B=C=D=E="defined at top-level"
    class Super
        A=B=C=D="defined in superclass"
    end
    module Included
        A=B=C="defined in included module"
    end
    module Enclosing
        A=B="defined in enclosing module"
        class Local < Super
            include Included
            A = "defined locally"
            search = (Module.nesting + self.ancestors + Object.ancestors).uniq # [Enclosing::Local, Enclosing, Included, Super, Object, Kernel]
            puts A
            puts B
            puts C
            puts D
            puts E
            puts F
        end
    end
    ```
    > **SALIDA**
    ```
    puts A -> "defined locally"
    puts B -> "defined in enclosing module"
    puts C -> "defined in included module"
    puts D -> "defined in superclass"
    puts E -> "defined at toplevel"
    puts F ->"defined in kernel"
    ```
    >Primero intenta resolver la constante en el ámbito local en el que ocurre la referencia. Si no, busca en el siguiente módulo o clase que anida en su ámbito la referencia a la constante y esto continúa hasta que no hay más clases o módulos que encierren a la referencia. Si no se encuentra la constante se pasa a buscar
    en la jerarquía de herencia. Si no se encuentra se buscan en las constantes globales. Si no se encuentra se llama al método const_missing. Se imprimirá cada constante que se encuentre antes.

    >El módulo Kernel está incluido dentro de la clase Object.

    >La variable search contiene: [Enclosing::Local, Enclosing, Included, Super, Object, Kernel].

    >http://crguezl.github.io/apuntes-ruby/node231.html

81. Considere el siguiente programa Ruby.:star:

    ```ruby
    class AClass 2
        def s
            ’a_s’
        end
        def g
            ’a_g’
        end
    end

    class OtherClass < AClass
    12    def s
            ’o_s’
        end
        def g
    16        super << " and o_g"
        end
    end
    ```
    * ¿Qué se hace en la línea 12?
        >Se invalida el método s de la clase AClass en la subclase Otherclass.

    * ¿Qué se hace en la línea 16?
        >Se invalida el método g de la clase AClass en la subclase Otherclass, donde se llama al método g de la clase padre y se le concatena " and o_g".

82. ¿Qué devuelve? ¿Por qué?:star:

    ```ruby
    def a_method
    end
    ```
    >Devuelve nil ya que el método no tiene código incluido y se devuelve la última sentencia ejecutada del método, en este caso nil.

83. ¿Qué devuelve? ¿Por qué?:star:

    ```ruby
    def a_method
        s = "message"
        a=1
        b = 2.0
        return a, b, 3, ’four’, s, 6*10
    end
    ```
    > **SALIDA**
    ```
    [1, 2.0, 3, "four", "message", 60].
    ```
    >Devuelve un array con todos los elementos (de distintos tipos, ya que Ruby consta de dicha funcionalidad por defecto)

84. ¿Qué devuelve? ¿Por qué? :star:

    ```ruby
    def a_method
        {’a’ => ’1’, ’b’ => ’2’, ’c’ => ’3’}
    end
    ```
    > **SALIDA**
    ```
    {’a’ => ’1’, ’b’ => ’2’, ’c’ => ’3’}
    ```
    >Devuelve el hash con todos los elementos definidos en la última línea. En Ruby siempre se devuelve la última línea definida aunque el método no conste de un return.

85. ¿Cuál es la salida? ¿Por qué? :star:

    ```ruby
    def a_method(a = 10, b = 20, c = 100, *d)
        return a,b,c,d
    end
    a_method(1,2,3,4,5,6)
    ```
    > **SALIDA**
    ```
    [1, 2, 3, [4, 5, 6]]
    ```
    >El método tiene como argumentos a, b, c y `*d` , donde a, b y c tienen valores por defecto si no se les pasa un valor y a d le precede el operador splat, que indica que puede que se pasen más parámetros al método. Estos últimos parámetros se agrupan y se crea un array con ellos. En la salida, [4, 5, 6] son los
    parámetros adicionales que recoge `*d`.

    >La salida es un array, ya que, al hacer un return de varias variables, se crea automáticamente un array con los valores de esas variables.

86. Señale el error de concepto en la codificación de la siguiente clase Ruby::star:

    ```ruby
    class MyClass
        def initialize(x,y)
            @x, @y = x, y
        end
        def polar(x,y)
            Math.hypot(y,x), Math.atan2(y,x)
        end
    end
    ```
    >El error que se está cometiendo es que en el método polar se quiere retornar más de un valor, y es necesario especificar explícitamente la palabra reservada return cuando se quiera retornar más de un valor.

87. Considere el siguiente programa Ruby. :star:

    ```ruby
    s = ’message 1’
    r = ’message 2’

    def s
        ’message from s’
    end

    def r
        ’message from r’
    end

    def t(x)
        "#{x}"
    end

  16  puts (r)
  17  puts r
  18  puts r()
  19  puts (t(s))
  20  puts (t(s()))
    ```
    > **SALIDA**
    ```
    message 2
    message 2
    message from r
    message 1
    message from s
    ```

    * ¿Qué permite diferenciar las líneas 16, 17 y 18?
        >La utilización de los paréntesis después de la variable, lo cual permite diferenciar si se desea llamar a la variable o al método.

    * ¿Qué diferencia hay entre las líneas 19 y 20?
        >La utilización de los paréntesis después de la variable, lo cual permite diferenciar si se desea llamar a la variable o al método.

88. ¿Qué comentario al comienzo del fichero permite usar caracteres unicode dentro del programa?:star:

    ># encoding: Unicode

89. ¿Qué comentario al comienzo del fichero permite usar caracteres UTF-8 dentro del programa?:star:

    ># encoding: utf-8

90. ¿En qué consiste el proceso de depuración (debugging)?

    >Se refiere al proceso de limpieza que se realiza en un programa para identificar y corregir errores o problemas de programación.

91. ¿Qué es un depurador (debugger)?

    >Es una herramienta para depurar de errores algún programa informático. Permite monitorizar la ejecución de un programa, asociando el código fuente con las instrucciones ejecutadas por el programa, detener la ejecución, seguir el flujo y comprobar los valores de las variables.

92. ¿Cómo se invoca al depurador de Ruby?

    >$ ruby –r debug nombre_fichero.rb

93. ¿Qué comando del depurador le permite enumerar las líneas de código fuente?

    >l[ist]

94. ¿Cómo se establece un punto de ruptura (breakpoint) con el depurador?

    >b[reak]

95. ¿Cómo se avanza hasta un punto de ruptura (breakpoint) con el depurador?

    >c[ont]

96. ¿Cómo se avanza con el depurador hasta la siguiente instrucción?:sos:

    >n[ext]

97. ¿Qué comando del depurador hay que utilizar para inspeccionar una variable?:sos:

    >p variable o display [nombre_variable]

98. ¿Qué comando del depurador permite ejecutar un método?

    >m[ethod]

99. ¿Qué comando del depurador muestra la pila de ejecución?

    >w[here] o f[rame]

100. ¿Qué comando del depurador permite reiniciar el contador de programa?

    >restart

101. ¿En qué consiste el proceso de construcción (build)?

    >Consiste en crear un conjunto de ficheros que se construyen a partir de otros.


102. ¿Qué es rake?

    >Rake (una gema Ruby) es una herramienta de gestión de tareas de software y de automatización de la compilación. Permite al usuario especificar tareas y describir dependencias, así como agrupar tareas en un espacio de nombres en un archivo llamado Rakefile. La herramienta está escrita en el lenguaje de programación Ruby.

103. ¿Cuál es la principal diferencia entre las herramientas de construcción genéricas como make y rake?

    >En el make el objetivo es construir un único ejecutable mediante varios archivos, mientras que en rake se definen tareas a realizar.

104. ¿Dónde se han de almacenar las tarea que ha de ejecutar rake?:star:

    >En un fichero llamado Rakefile.

105. ¿Cómo se describe una tarea en rake?
    ```ruby
    desc "One line task description"
      task :name_of_task do
      # Your code goes here
      end
    ```


106. ¿Para qué se utiliza la descripción de una tarea en rake?

    >Para identificar la tarea cuando se ejecute rake.

107. ¿Cómo se define un objetivo en rake?

    >Con dos puntos (:) delante del nombre del objeto (como un symbol):

    ```ruby
    task :nombre_objetivo do
    ...
    end
    ```

108. ¿Para qué se utiliza el objetivo de una tarea en rake?

    >El objetivo se utiliza para ejecutar la tarea. Ejemplo: rake test (ejecuta el objetivo 'test').

109. ¿Cómo se definen las acciones a ejecutar en rake?

    >En un bloque do-end.

110. ¿Cómo se definen la tarea por defecto en rake?:star:

    >task :default => [:my_task]

111. ¿Qué es la comprobación de unidades de código (Unit Testing)?

    >Las pruebas unitarias son una excelente forma de detectar errores de manera temprana en el proceso de desarrollo de software y una forma de comprobar su correcto funcionamiento. Es un método de prueba de software mediante el cual se realizan prueban unidades individuales de pequeños fragmentos de código fuente, para comprobar si funciona como lo esperado o no.

112. ¿En qué directorio hay que implementar las pruebas unitarias?:star:

    >Hay que implementarlas en el directorio test.

113. ¿Qué fichero se ha de incluir para implementar las pruebas unitarias?:star:

    >Hay que incluir la biblioteca 'test/unit'.

114. ¿Cómo se denomina la clase Ruby de la cual hay que heredar para implementar las pruebas?:star:

    >Test::Unit::TestCase

115. ¿Qué es un conjunto de pruebas (Test case)?:star:

    >Es un conjunto de condiciones o variables bajo las cuales un analista determinará si una aplicación, un sistema software  o una característica de éstos es parcial o completamente satisfactoria.

116. ¿Qué es una afirmación (assertion)?

    >Una afirmación es un predicado incluido en un programa como indicación de que el programador piensa que dicho predicado siempre se cumple en ese punto del flujo de programa.

117. Describa el comportamiento de la afirmación assert(<boolean expresion>) y proponga un
ejemplo de uso.

    >Comprueba si la expresión booleana es verdadera. Ejemplo: assert(number==2).


118. Describa el comportamiento de la afirmación assert_equal(expected, actual) y proponga un ejemplo de uso.

    >Comprueba si expected es igual a actual. Ejemplo: assert_equal(4, SimpleNumber.new(2).add(2)).


119. Describa el comportamiento de la afirmación assert_not_equal(expected, actual) y proponga un ejemplo de uso.

    >Comprueba si expected no es igual a actual. Ejemplo: assert_not_equal(array1,array2).

120. Describa el comportamiento de la afirmación assert_raise(exception_type,..){<code block>} y proponga un ejemplo de uso.

   >Comprueba si el bloque dado lanza una o más excepciones. Ejemplo: assert_raise(RuntimeError) {Point.new('1','1')}

121. Describa el comportamiento de la afirmación assert_not_raise(exception_type,..){<code block>} y proponga un ejemplo de uso.

    >Si se le pasan tipos de excepciones como parámetros, esta aserción fallará si se lanza alguna de las
    excepciones especficadas. Ejemplo: assert_raise(RuntimeError) {Point.new('1','1')}


122. Describa el comportamiento de la afirmación assert_instance_of(class_expected, object) y proponga un ejemplo de uso.

    >Comprueba si object es una instancia de class_expected.Ejemplo:  assert_instance_of(String,"foo").

123. Describa el comportamiento de la afirmación assert_respond_to(object, method) y proponga un ejemplo de uso.

    >Comprueba si object responde a un método method.

    >Ejemplo:
              * assert_respond_to("hello", :reverse)  #Succeeds
              * assert_respond_to("hello", :does_not_exist)  #Fails

124. Describa el comportamiento de la afirmación y proponga un ejemplo de uso.

    ```ruby
    assert_in_delta(expected_float, actual_float, delta)
    ```
    >Esta afirmación se pasa con éxito si  expected_float y real_float son iguales dentro de la tolerancia delta. Ejemplo: assert_in_delta( 0.05, (50000.0 / 10**6), 0.00001)


125. ¿Qué hace el método setup de la clase TestCase?

    >Este método se invoca antes de cada  test que se ejecuta. Se puede utilizar para declarar objetos que necesites en el test.

126. ¿Para qué se utiliza el método teardown de la clase TestCase?

    >Este método se invoca después de cada método de test que se ejecuta. En este método limpias los objetos contra los que probaste el test.

127. ¿Qué opción permite ejecutar sólo uno de los test unitarios implementados?

    >-n test

128. ¿Qué opción permite ejecutar los test unitatios cuyos nombres concuerdan con un patrón?

    >-n /patrón/

129. Escriba una tarea de Rake para lanzar las pruebas unitarias.
    ```ruby
    task :default => :tu
    desc "Pruebas unitarias de la clase Point"
      task :tu do
        sh "ruby -I. test/tc_point.rb"
      end
    ```

130. ¿En qué consiste el desarrollo dirigido por pruebas (Test Driven Development)?

    >Es un proceso de desarrollo de software que se basa en la idea de desarrollar primero las  pruebas, desarrollar el código que las resuelve y refactorizar dicho código.

131. ¿Cómo se denomina el paradigma de desarrollo en el que se basa la herramienta RSpec?

    >Se basa en el desarrollo guiado por el comportamiento (BDD).

132. Describa el conjunto de pasos a seguir para desarrollar una aplicación con RSpec.

    >En primer lugar, se escribe una prueba y se verifica que las pruebas fallan. A continuación, se implementa el código que hace que la prueba pase satisfactoriamente y seguidamente se refactoriza el código escrito. El propósito del desarrollo guiado por pruebas es lograr un código limpio que funcione.

133. ¿En qué directorio hay que implementar las especificaciones de las expectativas de una clase?

    >En la carpeta spec.

134. ¿Qué hace el método describe?

    >Se utiliza para definir un grupo de expectativas.

135. ¿Qué hace el método it?

    >Se utiliza para definir una expectativa.

136. ¿Qué hace el método before? ¿Qué significado tienen :each y :all?

    >before declara un bloque de código que se ejecutará antes de cada expectativa o una vez antes de cualquier expectativa. Con :each el bloque de código se ejecuta antes de cada expectativa y con :all se ejecuta sólo una vez antes de todas las expectativas de un grupo.


137. ¿Qué significado tiene la expectativa expect(result).to eq(5)?

    >El valor de la variable result debe ser 5.

138. ¿Qué significado tiene la expectativa expect([1,2,3]).to include(2)?

    >El número 2 debe estar incluido en el array [1,2,3].

139. ¿Qué significado tiene la expectativa expect { a += 1 }.to change { a }.by(1) ?

    >Se espera que la variable 'a',  al sumarle un 1, se vea modificado su valor incrementándose en 1 unidad.

140. ¿Qué opción permite ejecutar RSpec de manera que muestre por consola la descripción de cada una de las especificaciones realizadas?

    >--format documentation.

141. Escriba una tarea de Rake para lanzar las pruebas de RSpec.
    ```ruby
    desc "Ejecutar las espectativas de la clase Lista"
       task :spec do
      sh "rspec -I. spec/lista_spec.rb"
      end
    ```

142. ¿Qué es RubyGem?

    >Es un gestor de paquetes para el lenguaje de programación Ruby que proporciona un formato estándar y autocontenido (llamado gema) para poder distribuir programas o bibliotecas en Ruby, una herramienta destinada a gestionar la instalación de éstos, y un servidor para su distribución.

143. ¿Qué es una Gema (Gem)?

    >Es un conjunto de directorios con ficheros fuente Ruby que cumplen una serie de requisitos.

144. ¿Qué comando permite manipular gemas (gems)?

    >gem <opciones>

145. ¿Cómo se puede encontrar una gema (gem) en la maquina local? y ¿en remoto?

    * `Local`: gem list --local
    * `Remota`: gem list --remote o gem search [REGEXP] [options]

146. ¿Cómo se denomina el repositorio estándar de gemas (gems)?

    >RubyGems

147. ¿Qué comando permite manipular la instalación de gemas (gems)?

    >gem install gema

148. ¿Qué comando permite actualizar a la última versión una gema (gem)?

    >gem update gema

149. ¿Qué comando permite manipular la desinstalación de gemas (gems)?

    >gem uninstall gema

150. ¿Cómo se hace uso de una gema (gem) ya instalada?

    >En el código del programa, añadiendo "require 'gema'.

151. ¿Qué es bundler?

    >Es un gestor de dependencias de Ruby que proporciona un entorno coherente para los proyectos de Ruby mediante el seguimiento y la instalación de las gemas exactas y las versiones que se necesitan.

152. ¿Qué estructura de directorios genera la orden bundle gem ejemplo?
```ruby
├── bin
│   ├── console
│   └── setup
├── ejemplo.gemspec
├── Gemfile
├── lib
│   ├── ejemplo
│   │   └── version.rb
│   └── ejemplo.rb
├── Rakefile
├── README.md
└── spec
    ├── ejemplo_spec.rb
    └── spec_helper.rb
-------.git
-------.gitignore
```


153. Describa el contenido del fichero Gemfile que se genera por defecto.

    >Es un fichero donde se especifican las dependencias (gemas) requeridas de la gema que quieres utilizar.

154. Describa el contenido del fichero ejemplo.gemspec que se genera por defecto.

    >Es un fichero que contiene metadatos sobre la gema; es donde escribes las dependencias de la gema que quieres utilizar.Se introducen automáticamente en Gemfile.

155. Describa el contenido del fichero Rakefile que se genera por defecto.

    >Es un fichero de construcción para Ruby. En este fichero el usuario especifica tareas, describe dependencias, agrupa tareas en un espacio de nombres.

156. Describa el contenido del fichero version.rb que se genera por defecto.

    >Este fichero guarda la versión de la gema.

157. ¿Cuál es el comando para crear la gema (gem) ejemplo? ¿Qué se genera al ejecutarlo?

    >bundle gem ejemplo.

    >Se genera el directorio de la gema creada (ya está creado como repositorio git).

158. ¿Cuál es el comando para publicar la gema (gem) ejemplo? ¿Qué se hace al ejecutarlo?:sos:

    >gem push ejemplo.gem

    >O:
        * Para construir la gema, desde el directorio raíz creado con Bundler ejecutar: rake build
        * Para crear la version 0.1.0 de la gema ejecutar: rake release
        * Para instalar gema ejecutar: rake install
        * Para comprobrar que ha ido bien la instalación , ejecutar: gem ejemplo

159. ¿En qué directorio hay que colocar el fichero de configuración de la gema (gem)?

    >En el directorio raíz de la gema

160. ¿En qué directorio hay que colocar los ficheros con los códigos fuente Ruby de la gema (gem)?

    >En 'lib/gema'.

161. ¿En que consiste la comprobación continua Continous Testing?

    >Es un modelo informático que consiste en hacer integraciones automáticas de un proyecto lo más a menudo posible
    para así poder detectar fallos cuanto antes. Consiste en realizar un conjunto de pruebas de forma automática en distintas plataformas y versiones de Ruby cada vez que se incorpora una funcionalidad nueva en el código.

162. ¿Qué es Guard?

    >Es una herramienta de línea de comandos para manejar fácilmente eventos en las modificaciones del sistema de archivos. Permite ejecutar todas las expectativas, de forma automática, cada vez que se produce una modificación en el código fuente.


163. Describa el conjunto de pasos a seguir para usar Guard en el desarrollo de una aplicación.

    > 1. Se añade Guard como una dependencia de desarrollo en el fichero de especificaciones de la gema .gemspec.

    >      spec.add_development_dependency "guard" ,
          spec.add_development_dependency "guard-rspec" ,
          spec.add_development_dependency "guard-bundler" ,

    > 2. Se instala Guard a través de Bundler. -> bin/setup

    >3. Se procede a generar un fichero de guardia por defecto (Guardfile). -> bundle exec guard init

    >4. Se ejecuta Guard a través de Bundler en una terminal. -> bundle exec guard

    >5.En otra consola, cambias el fichero del código de las pruebas y, automáticamente, en la consola donde se inició Guard se ejecutan las pruebas.

164. ¿En qué directorio hay que implementar los requerimientos de Guard?

    >En el directorio raíz de la gema.

165. ¿Qué se ha de especificar en el fichero Guardfile?:star:

    >En este fichero se definen los ficheros de los que Guard va a hacer un seguimiento contínuo.

166. Describa el contenido de un fichero de Guardfile utilizado por Guard.:star:

    ```ruby
    guard :rspec do
    watch("fichero") {bloque}
    end
    ```
    >El método watch dice a Guard los ficheros modificados a los que el plugin debe responder.

    >El bloque se utiliza por si quieres modificar el nombre del archivo que ha sido detectado antes de enviarlo al plugin para procesarlo.

167. ¿Cómo se pone en funcionamiento Guard?

    >bundle exec guard init para generar un Guardfile y bundle exec guard para iniciar Guard.

168. ¿Qué es RDoc?

    > Es una herramienta de generación de documentación automática. Parsea los ficheros y extrae las definiciones de
    clases, módulos y métodos (junto con includes y requires), y asocia todo esto con la documentación opcional contenida en un bloque de comentarios inmediatamente por encima de clases, módulos y métodos. Una vez hecho esto, renderiza el resultado utilizando un formateador de salida.


169. La herramienta RDoc incluida de manera estándar en Ruby puede procesar fichero de código fuente Ruby y bibliotecas de clases en código fuente C para extraer documentación y formatearla de manera que se pueda mostrar en un navegador. ¿Verdadero o falso? ¿Por qué?:sos:

    >Verdadero ya que este comando genera documentación para todos los archivos de origen de Ruby y C y no hace falta instalarlo para utilizarlo, ya que viene incluida de manera estándar.

170. ¿De dónde extrae la información RDoc?

    >RDoc parsea los ficheros y extrae las definiciones de clases, módulos y métodos (junto con includes y requires), y asocia todo esto con la documentación opcional contenida en un bloque de comentarios inmediatamente por encima de clases, módulos y métodos.


171. El siguiente comentario es ignorado por RDoc. ¿Verdadero o falso? ¿Por qué?

    ```ruby
    =begin
    Esto es un comentario RDoc
    que ocupa varias líneas
    =end
    ```
    >Falso ya que es un comentario multilínea (es una cabecera de primer nivel).

172. ¿Cuál es el comando que pone en funcionamiento a RDoc?

    >rdoc.

173. ¿Dónde se aloja por defecto la documentación generada por RDoc?

    >Se almacenará en un árbol de documentación que comienza en el subdirectorio 'doc'.

174. Describa la documentación generada por RDoc.

    > RDoc produce documentación en formato HTML y CSS  para representar en un navegador la descripción de las clases y sus métodos.

175. El siguiente comentario es ignorado por RDoc. ¿Verdadero o falso? ¿Por qué?

    ```ruby
    #--
    # Este comentario no aparecerá
    # en la documentación
    #--
    # Este comentario si aparece
    ```

    >Verdadero ya que #-- indica a RDoc que a partir de esta sentencia deje de procesar los comentarios, y para que el comentario ""#Este comentario sí aparece" debería aparecer la sentencia #++, pero vuelve a aparecer la sentencia #--.


176. Describa el significado de la línea 7.

    ```ruby
    # Esta es una clase que no hace nada
    class AClass
        #Estaesladocumentacióndeestemétodo
        def a_method_1
        end

        def a_method_2 #:nodoc:
        end
    end
    ```

    >Con la directiva :nodoc  puede ocultar una clase o método de la documentación. De esta forma, el método a_method_2 será ignorado por RDoc.

177. RDoc no procesa los comentarios que están dentro de los métodos por lo tanto no aparecerán en la documentación producida. ¿Verdadero o falso? ¿Por qué?

    >Verdadero puesto que los comentarios dentro de los métodos están formateados como código fuente Ruby, no como marcas RDoc.Además, RDoc va en busca de los comentarios encima de la definición de cada función, constante, método, clase, etc., pero no en el interior de un método.

178. ¿Para qué se utiliza la opción --all de la herramienta RDoc?

    >Se utiliza para incluir los métodos protegidos y privados en la salida del formateador utilizado por RDoc , ya que por defecto solo se incluyen métodos públicos.
